# RappiTest

## Objetivo
Crear un sitio web, con cualquier framework para frontend, responsive, que cargue los datos de un archivo json, configurable para modificar la ruta de donde se obtiene el json (local o url).
Esos datos seran mostrados en el sitio, el cual se les pueden aplicar una lista de filtros. Ademas, se puede buscar un producto individual.

```
Filtros:
1. Por Categorias
2. Por Productos disponibles
3. Por Productos agotados
4. Por Productos mas vendidos
5. Por Productos de precio mayor a 30.000
6. Por Productos de precio menor a 10.000
7. Ordenar por nombre
8. Ordenar por mayor precio
9. Ordenar por menor precio
```
Posteriormente, se debe crear un carrito de compras, donde se puedan agregar y eliminar productos.

## Inicio

El proyecto fue desarrollador con el framework de desarrollo para frontend AngularJS y el framework CSS MaterializeCSS.

La persistencia de datos para el carrito de compras, fue manejado con localStorage. (Los fuentes se encuentran en la seccion "Construido con").

La estructura del proyecto, estructura de controladores, servicios, filtros, usados fueron tomadas de las buenas practicas de AngularJS de Johnpapa - angular-styleguide https://github.com/johnpapa/angular-styleguide/tree/master/a1.

### Prerequisitos

Un servidor local, para ejecutar este proyecto.

```
http://localhost/rappi/app/#!/
```

## Clonar repositorio
git clone https://gitlab.com/nicolasq92/rappiTest.git


## Ejecucion
Puedes acceder a este repositorio, realizar un pull request, y colgarlo en tu servidor (ejm. apache).

o

Acceder a la siguiente direccion:

```
rappi.anubistech.com.ve/
```

## Dessarrollado con

* [AngularJS](https://github.com/angular/angular.js) - El Framework para frontend usado
* [MaterializeCSS](https://github.com/Dogfalo/materialize) - Framework CSS usado
* [angular-materialize](https://github.com/krescruz/angular-materialize) - JS para efectos y animaciones de Materialize
* [ngStorage](https://github.com/gsklee/ngStorage) - usado para mantener la persistencia de datos en el carrito de compras.



## Authors

* **Jose Nicolas Quijada P** - *Initial work*

## License

Este proyecto no esta bajo ninguna licencia. es solo para fines de evaluacion por parte del equipo de Rappi.

## Fe de Errata.

Uno de los elementos del Archivo .json fue entregado de la siguiente manera:

```
{
  "id": 8,
  "name": "elit",
  "price": "2.000", <---- original : 2000
  "available": true,
  "best_seller": false,
  "categories": [
    1,
    3
  ],
  "img": "http://lorempixel.com/200/100/food/",
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu."
},
```
El valor fue modificado, colocado con el mismo formato de los otros numeros.

## Capturas del sitio

### Principal

![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A24%3A27.png)
![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A28%3A57.png)
![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A24%3A41.png)
![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A42%3A15.png)


### Carrito de Compras

![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A25%3A54.png)


### Responsive

![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A28%3A10.png)
![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A28%3A31.png)


### Estructura del Sitio

![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A20%3A44.png)

![alt tag](https://dl.dropboxusercontent.com/u/27081989/Rappi/Captura%20de%20pantalla%20de%202017-01-20%2023%3A17%3A07.png)
